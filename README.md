# Setup my laptop with ansible

To setup the whole system, from a fresh Arch installation:
```bash
$ ansible-playbook --ask-become-pass --ask-vault-pass setup_laptop.yml
```

Just tasks that can be executed by user `blue`, without admin privileges:
```bash
$ ansible-playbook --tags blue setup_laptop.yml
```

## Reproducing my environment in a Docker container

The only caveat is that you don't have my vault password, so in `setup_laptop.yml`
you have to change `{{ blue_pw }}` and `{{ blue_ssh_pw }}` to your own custom strings
and remove `vault/blue_passwords.yml` from the `vars_files`.

```
$ docker run --rm -it archlinux:latest
# pacman -Syu git ansible
# git clone https://gitlab.com/eguiraud/laptopsetup.git
# cd laptopsetup
# ansible-playbook --ask-become-pass setup_laptop.yml
```
