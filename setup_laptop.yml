- name: configure system
  hosts: localhost
  connection: local
  gather_facts: false
  become: true
  tags: system
  vars_files:
    - packages.yml
    - vault/blue_passwords.yml

  tasks:
    - name: install packages
      pacman:
        name: '{{ base + window_manager + network + utils + audio + cern_root }}'

    - name: configure X11
      shell:
        cmd: Xorg -configure && mv /root/xorg.conf.new /etc/X11/xorg.conf
        creates: /etc/X11/xorg.conf

    - name: configure touchpad
      blockinfile:
        path: /etc/X11/xorg.conf.d/40-libinput.conf
        create: true
        block: |
          Section "InputClass"
                  Identifier "libinput touchpad catchall"
                  MatchDriver "libinput"
                  MatchIsTouchpad "on"
                  Option "Tapping" "on"
                  Option "ClickMethod" "clickfinger"
                  MatchDevicePath "/dev/input/event*"
          EndSection

    # disabled for now as there was a problem with the webkit2-greeter last time I tried
    #- name: configure lightdm
    #  lineinfile:
    #    path: /etc/lightdm/lightdm.conf
    #    line: greeter-session=lightdm-webkit2-greeter
    #    regexp: '^#?greeter-session='

    - name: ensure required services are enabled
      service:
        name: '{{ item }}'
        enabled: true
      loop:
        #- lightdm
        - NetworkManager
        - bluetooth
        - cups

    - name: create user blue
      user:
        name: blue
        groups: docker
        password: '{{ blue_pw | string | password_hash("sha512") }}'
        generate_ssh_key: true
        ssh_key_passphrase: '{{ blue_ssh_pw }}'
        shell: /usr/bin/fish
        update_password: on_create

    - name: add blue to sudoers
      community.general.sudoers:
        name: blue-as-root
        state: present
        user: blue
        runas: ALL
        commands: ALL
        nopassword: false

    - name: let blue run pacman without password
      community.general.sudoers:
        name: blue-pacman-nopw
        user: blue
        commands:
          - /usr/bin/pacman
        nopassword: true

- name: configure root user
  tags: system, root
  hosts: localhost
  gather_facts: false
  connection: local
  become: true
  tasks:
    - name: add better aliases for rm, mv, ls
      blockinfile:
        path: /root/.profile
        create: true
        block: |
          alias rm="rm -i"
          alias mv="mv -i"
          alias lr="ls -tr"
          alias ls="ls --color=auto"

- name: configure user blue
  tags: blue
  hosts: localhost
  connection: local
  gather_facts: false
  become: true
  become_user: blue
  vars:
    home: /home/blue
    repos: '{{ home }}/Repos'
    conf_repo: '{{ repos }}/dotfiles'
    configs: '{{ home }}/.config'

  tasks:
    - name: download conf repo
      git:
        repo: https://gitlab.com/eguiraud/dotfiles.git
        dest: '{{ conf_repo }}'
        update: false
        version: main

    - name: create dirs for dotfiles
      file:
        path: '{{ item }}'
        state: directory
      loop:
        - '{{ configs }}/i3'
        - '{{ configs }}/i3status-rust'
        - '{{ configs }}/git'
        - '{{ configs }}/ccache'
        - '{{ configs }}/systemd/user'
        - '{{ configs }}/alacritty'
        - '{{ configs }}/ripgrep'
        - '{{ configs }}/dunst'
        - '{{ configs }}/helix'
        - '{{ configs }}/sway'
        - '{{ home }}/.gnupg'
        - '{{ home }}/.ssh'
        - '{{ home }}/.vim'
        - '{{ home }}/.vim/autoload'
        - '{{ home }}/.gdbinit.d'

    - name: configure dotfiles
      file:
        src: '{{ item.src }}'
        dest: '{{ item.dest }}'
        state: link
      loop:
        - { src: '{{ conf_repo }}/confs/vimrc', dest: '{{ home }}/.vimrc' }
        - { src: '{{ conf_repo }}/confs/gitconfig', dest: '{{ configs }}/git/config' }
        - { src: '{{ conf_repo }}/git_template', dest: '{{ configs }}/git/templates' }
        - { src: '{{ conf_repo }}/confs/i3config', dest: '{{ configs }}/i3/config'}
        - { src: '{{ conf_repo }}/confs/i3status-rs-config.toml', dest: '{{ configs }}/i3status-rust/config.toml'}
        - { src: '{{ conf_repo }}/fish', dest: '{{ configs }}/fish'}
        - { src: '{{ conf_repo }}/confs/rofi', dest: '{{ configs }}/rofi'}
        - { src: '{{ conf_repo }}/confs/profile', dest: '{{ home }}/.profile'}
        - { src: '{{ conf_repo }}/confs/gpg-agent.conf', dest: '{{ home }}/.gnupg/gpg-agent.conf'}
        - { src: '{{ conf_repo }}/confs/wiki.vim', dest: '{{ home }}/.vim/wiki.vim'}
        - { src: '{{ conf_repo }}/confs/taskrc', dest: '{{ home }}/.taskrc'}
        - { src: '{{ conf_repo }}/confs/sshconfig', dest: '{{ home }}/.ssh/config'}
        - { src: '{{ conf_repo }}/confs/ccache.conf', dest: '{{ configs }}/ccache/ccache.conf'}
        - { src: '{{ conf_repo }}/confs/gdb-dashboard.conf', dest: '{{ home }}/.gdbinit.d/dashboard'}
        - { src: '{{ conf_repo }}/confs/user-dirs.dirs', dest: '{{ configs }}/user-dirs.dirs'}
        - { src: '{{ conf_repo }}/scripts', dest: '{{ home }}/bin'}
        - { src: '{{ conf_repo }}/systemd/user/ssh-agent.service', dest: '{{ configs }}/systemd/user/ssh-agent.service'}
        - { src: '{{ conf_repo }}/systemd/user/backup.path', dest: '{{ configs }}/systemd/user/backup.path'}
        - { src: '{{ conf_repo }}/systemd/user/backup.service', dest: '{{ configs }}/systemd/user/backup.service'}
        - { src: '{{ conf_repo }}/confs/alacritty.yml', dest: '{{ configs }}/alacritty/alacritty.yml'}
        - { src: '{{ conf_repo }}/confs/rg.conf', dest: '{{ configs }}/ripgrep/rg.conf'}
        - { src: '{{ conf_repo }}/confs/dunstrc', dest: '{{ configs }}/dunst/dunstrc'}
        - { src: '{{ conf_repo }}/confs/helix.toml', dest: '{{ configs }}/helix/config.toml'}
        - { src: '{{ conf_repo }}/confs/sway.config', dest: '{{ configs }}/sway/config'}

    - name: install vim plug
      get_url:
        url: 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
        dest: '{{ home }}/.vim/autoload/plug.vim'

    - name: install vim plugins
      shell:
        cmd: 'nvim +PlugInstall +qall && touch {{ home }}/.vim/.ansible_installed_plugins'
        creates: '{{ home }}/.vim/.ansible_installed_plugins'

    - name: set firefox as default browser for xdg-open # because chromium sets this
      shell:
        cmd: 'xdg-settings set default-web-browser firefox.desktop'

    - name: set evince as default PDF opener # because chromium sets this
      shell:
        cmd: 'xdg-mime default org.gnome.Evince.desktop application/pdf'

    - name: configure gdb dashboard
      get_url:
        url: https://git.io/.gdbinit
        dest: '{{ home }}/.gdbinit'

    - name: ensure Repos dir exists
      file:
        path: '{{ home }}/Repos'
        state: directory

    - name: git clone yay
      git:
        repo: https://aur.archlinux.org/yay.git
        dest: '{{ repos }}/yay'
        update: false

    - name: install yay
      command:
        chdir: '{{ repos }}/yay'
        cmd: 'makepkg -si --noconfirm'
        creates: /usr/bin/yay

    - name: install autojump with yay
      command:
        cmd: 'yay -Syu --noconfirm autojump'
        creates: /usr/bin/autojump

    - name: ruby support in nvim
      command:
        cmd: 'gem install neovim'

    - name: enable ssh-agent service
      systemd:
         name: 'ssh-agent.service'
         enabled: true
         state: 'started'
         scope: 'user'

    - name: enable back-up service
      systemd:
         name: 'backup.path'
         enabled: true
         scope: 'user'
